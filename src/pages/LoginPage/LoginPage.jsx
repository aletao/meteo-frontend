import { useState, useContext, useEffect } from "react";
import { AuthContext } from "../../contexts/AuthContext";
import styles from "./LoginPage.module.css";
import { useNavigate } from "react-router-dom";
import { regexMatcher } from "../../service/RegexMatcher";
import { login } from "../../service/AuthService";
import Cookies from "js-cookie";


export default function LoginPage() {
  const [loginUser, setLoginUser] = useState({
    email: "",
    password: "",
  });


  const [loginError, setLoginError] = useState(false);

  const { user, setUser } = useContext(AuthContext);

  const { isAuth, setIsAuth } = useContext(AuthContext);

  const navigate = useNavigate();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setLoginUser((prevUser) => ({
      ...prevUser,
      [name]: value,
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if(regexMatcher(loginUser.email)){
        const response = await login(loginUser);
        if (response.status === 200) {
          const data = await response.json();
          setUser(data.user);
          setIsAuth(true);
          Cookies.set("token", data.token);
          navigate("/");
        } 
    }
    setLoginError(true);
  };

  const handleRegisterClick = () => {
    navigate("/register");
  };

  useEffect(() => {
    
  }, [loginError]);

  return (
    <>
      <div className={styles.centerDiv}>
        <img src="./meteoimage.png" alt="logo" />
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <label htmlFor="exampleInputEmail1" className="form-label">
              Email address
            </label>
            <input
              type="email"
              className="form-control"
              id="exampleInputEmail1"
              name="email"
              value={loginUser.email}
              onChange={handleChange}
              aria-describedby="emailHelp"
            />
          </div>
          <div className="mb-3">
            <label htmlFor="exampleInputPassword1" className="form-label">
              Password
            </label>
            <input
              type="password"
              className="form-control"
              id="exampleInputPassword1"
              name="password"
              value={loginUser.password}
              onChange={handleChange}
            />
            <div style={{marginTop:"5%"}}>
              <span onClick={handleRegisterClick} style={{
                  textDecoration: "underline blue",
                  color: "blue",
                  cursor: "pointer",
                }}>You dont have an account? get one!</span>
            </div>
          </div>
          {loginError && (
            <div id="emailHelp" className="form-text" style={{marginBottom:"1vh"}}>
              <span className="text-danger">Wrong email or password!</span>
            </div>
          )}
          <button type="submit" className="btn btn-primary">
            Login
          </button>
        </form>
      </div>
    </>
  );
}
