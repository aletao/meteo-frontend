import styles from "./RegisterPage.module.css";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { register } from "../../service/AuthService";

export default function RegisterPage() {
  const [showError, setShowError] = useState(false);

  const [registerUser, setRegisterUser] = useState({
    name: "",
    lastname: "",
    email: "",
    password: "",
  });

  const navigate = useNavigate();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setRegisterUser((prevUser) => ({
      ...prevUser,
      [name]: value,
    }));
  };

  const handleSubmit = async (e) => {

    e.preventDefault();

    const reponse = await register(registerUser);
    console.log(registerUser);
    if (reponse.status === 200) {
      navigate("/login");
    } else {
      setShowError(true);
    }
  };

  const handleLoginClick = () => {
    navigate("/login");
  };

  useEffect(() => {}, [showError]);

  return (
    <>
      <div className={styles.centerDiv}>
        <div className={styles.formContainer}>
          <img
            src="./meteoimage.png"
            alt="logo"
            className={styles.imageStyle}
          />
          <form onSubmit={handleSubmit}>
            <div className={styles.firstSection}>
              <div>
                <label htmlFor="name" className="form-label">
                  Name
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="name"
                  name="name"
                  onChange={handleChange}
                  aria-describedby="emailHelp"
                  required={true}
                  style={{ width: "90%" }}
                />
              </div>
              <div>
                <label htmlFor="lastname" className="form-label">
                  Lastname
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="lastname"
                  name="lastname"
                  onChange={handleChange}
                  aria-describedby="emailHelp"
                  required={true}
                />
              </div>
            </div>
            <div className="mb-3">
              <label htmlFor="email" className="form-label">
                Email address
              </label>
              <input
                type="email"
                className="form-control"
                id="email"
                name="email"
                onChange={handleChange}
                aria-describedby="emailHelp"
                required={true}
              />
            </div>
            <div className="mb-3">
              <label htmlFor="password" className="form-label">
                Password
              </label>
              <input
                type="password"
                className="form-control"
                id="password"
                name="password"
                onChange={handleChange}
              />
            </div>
            <div id="passwordHelp" className="form-text">
              Password must contain at least 8 characters,
            </div>
            <div style={{ marginTop: "1vh", marginBottom: "1vh" }}>
              <span
                onClick={handleLoginClick}
                style={{
                  textDecoration: "underline blue",
                  color: "blue",
                  cursor: "pointer",
                }}
              >
                Already registered?
              </span>
            </div>
            <button type="submit" className="btn btn-primary">
              Register
            </button>
            {showError && (
              <div style={{ textAlign: "center" }}>
                <span className="text-danger">
                  Error during the registration
                </span>
              </div>
            )}
          </form>
        </div>
      </div>
    </>
  );
}
