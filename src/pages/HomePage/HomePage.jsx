import { useContext } from "react";
import { AuthContext } from "../../contexts/AuthContext";
import { useNavigate } from "react-router-dom";
import styles from "./HomePage.module.css";
import { useState } from "react";
import {
  getRightImage,
  searchForecast,
  createForecast,
} from "../../service/ForecastRequestService";

export default function HomePage() {
  const { user } = useContext(AuthContext);

  const [city, setCity] = useState("");

  const { isAuth } = useContext(AuthContext);

  const [forecast, setForecast] = useState([]);

  const [showError, setShowError] = useState(false);

  const navigate = useNavigate();

  console.log(user);

  const handleLogin = () => {
    navigate("/login");
  };

  const handleRegister = () => {
    navigate("/register");
  };

  const handleChange = (e) => {
    setCity(e.target.value);
  };

  const handleSearch = async () => {
    if (!isAuth) {
      var response = await searchForecast(city);
    } else {
      response = await createForecast(city, user.email);
    }
    const tempForecast = [];
    response.weatherConditions.map((element) => {
      const forecastElement = {
        latitude: element.latitude,
        longitude: element.longitude,
        city: element.city,
        date: element.date,
        minTemperature: element.minTemperature,
        maxTemperature: element.maxTemperature,
        weatherStatus: element.weatherStatus,
        weatherCode: element.weatherCode,
      };
      tempForecast.push(forecastElement);
    });

    console.log(tempForecast);
    setForecast(tempForecast);
  };

  return (
    <>
      <div className={styles.centerDiv}>
        {!isAuth && (
          <div className={styles.topWarning}>
            <div className="card">
              <div className="card-header text-warning">Warning</div>
              <div className="card-body">
                <h5 className="card-title">Login or Register</h5>
                <p className="card-text">
                  Create an account to start saving your forecast requests!
                </p>
                <div className={styles.buttonContainer}>
                  <button
                    onClick={handleLogin}
                    className="btn btn-primary"
                    style={{ margin: "1%" }}
                  >
                    Login
                  </button>
                  <button
                    onClick={handleRegister}
                    className="btn btn-primary"
                    style={{ margin: "1%" }}
                  >
                    Register
                  </button>
                </div>
              </div>
            </div>
          </div>
        )}

        <div className={styles.cardSection}>
          <h1 style={{ textAlign: "center", marginTop: "5%" }}>
            Search your city!
          </h1>
          <div className="input-group">
            <input
              type="text"
              style={{ marginRight: "2%" }}
              className="form-control"
              placeholder="City name"
              id="city"
              name="city"
              value={city}
              onChange={handleChange}
            />
            <div className="input-group-append">
              <button
                className="btn btn-primary"
                type="button"
                onClick={handleSearch}
              >
                Search
              </button>
            </div>
          </div>
          {showError && (
            <div
              id="emailHelp"
              className="form-text"
              style={{ marginBottom: "1vh" }}
            >
              <span className="text-danger">Wrong city input!</span>
            </div>
          )}
          {forecast.map((element, index) => (
            <div
              className="card text-bg-dark"
              style={{ marginTop: "2%", textAlign: "center" }}
            >
              <img
                src={getRightImage(element.weatherCode)}
                className="card-img"
                alt="background"
                style={{ height: "200px", objectFit: "cover" }}
              />
              <div className="card-img-overlay">
                <h5 className="card-title text-dark">
                  {element.city} - {element.date}
                </h5>
                <p className="card-text text-dark" style={{fontWeight: "bold"}}>
                  Min: {element.minTemperature}°C - Max:{" "}
                  {element.maxTemperature}°C
                </p>
                <p className="card-text text-dark" style={{fontWeight: "bold"}}>
                  Weather: {element.weatherStatus}
                </p>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}
