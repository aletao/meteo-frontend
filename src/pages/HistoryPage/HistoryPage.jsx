import React from "react";
import styles from "./HistoryPage.module.css";
import { useState } from "react";
import { getForecastHistory } from "../../service/ForecastRequestService";
import { useContext, useEffect } from "react";
import { AuthContext } from "../../contexts/AuthContext";

export default function HistoryPage() {
  const { user } = useContext(AuthContext);

  const [forecasts, setForecasts] = useState([]);

  const getForecasts = async () => {
    const response = await getForecastHistory();

    const tempForecasts = [];
    response.weatherConditions.map((element) => {
      const forecastElement = {
        id: element.id,
        requestDate: element.requestDate,
      };
      tempForecasts.push(forecastElement);
    });

    setForecasts(tempForecasts);
  };

  useEffect(() => {
    getForecasts();
  }, []);

  return (
    <>
      <div className={styles.centerDiv}>
        <h1>History of your forecast searches!</h1>
        <div className="container">
          <div className="row">
            <div className="col-md">
              <table className="table table-dark table-hover table-bordered rounded">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Request Date</th>
                  </tr>
                </thead>
                <tbody>
                  {forecasts.map((forecast, index) => (
                    <tr key={index}>
                      <td>{forecast.id}</td>
                      <td>{forecast.requestDate}</td>
                      <td>
                        <button
                          type="button"
                          className="btn btn-outline-danger"
                        >
                          View
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
