import { useOutlet } from "react-router-dom";
import Navbar from "../components/Navbar/Navbar";

export default function Navigation() {
  const outlet = useOutlet();
  //mettere footer e navbar
  return (
    <>
      <Navbar />
      {outlet}
    </>
  );
}
