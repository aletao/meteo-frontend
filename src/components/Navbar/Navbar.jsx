import React, { useContext, useEffect } from "react";
import { AuthContext } from "../../contexts/AuthContext";
import { useNavigate } from "react-router-dom";
import { NavLink } from "react-router-dom";

export default function Navbar() {
  const { isAuth } = useContext(AuthContext);

  const navigate = useNavigate();

  useEffect(() => {}, [isAuth]);

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top ">
        <div className="container-fluid">
          <a className="navbar-brand" href="#">
            WeatherForecast.com
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item">
              <NavLink
                  to="/"
                  className="nav-link"
                  activeclassname="active"
                  aria-current="page">
                  Home
                </NavLink>
              </li>
              {isAuth && (
                <li className="nav-item">
                  <NavLink
                    to="/History"
                    className="nav-link"
                    activeclassname="active"
                    aria-current="page"
                  >
                    History
                  </NavLink>
                </li>
              )}
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}
