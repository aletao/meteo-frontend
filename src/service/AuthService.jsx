export const login = async (loginUser) => {
    const response = await fetch("http://localhost:8080/api/user/login", {
        method: "POST",
        body: JSON.stringify(loginUser),
        headers: {
            "Content-type": "application/json; charset=UTF-8" 
        }
    });
    
    return response;
}

export const register = async (registerUser) => {
    const response = await fetch("http://localhost:8080/api/user/register", {
        method: "POST",
        body: JSON.stringify(registerUser),
        headers: {
            "Content-type": "application/json; charset=UTF-8" 
        }
    });

    return response;
}