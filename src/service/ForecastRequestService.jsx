import Cookies from "js-cookie";

export const searchForecast = async (city) => {
  const response = await fetch("http://localhost:8080/api/forecast/search?city=" + city, {
    method: "GET",
    headers: {
      "Content-type": "application/json; charset=UTF-8",
    },
  });

  const responseData = await response.json();

  return responseData;
};

export const createForecast = async (city, userEmail) => {
  console.log(Cookies.get("asdasdsa" + "token"));
  const response = await fetch("http://localhost:8080/api/forecast/get?city=" + city + "&email=" + userEmail, {
    method: "GET",
    headers: {
      "Content-type": "application/json; charset=UTF-8",
      "Authorization": "Bearer " + Cookies.get("token"),
    },
  });

  const responseData = await response.json();

  return responseData;
};

export const getForecastHistory = async (email) => {
  const response = await fetch("http://localhost:8080/api/forecast/get/all?email=" + email, {
    method: "GET",
    headers: {
      "Content-type": "application/json; charset=UTF-8",
    },
  });

  const responseData = await response.json();

  console.log(responseData);

  return responseData;
}



export const getRightImage = (weatherCode) => {
    switch (weatherCode) {
      case 0:
      case 1:
      case 2:
        return "sunny.avif";
      case 3:
        return "cloudy.jpg";
      case 45:
      case 48:
        return "fog.jpg";
      case 51:
      case 53:
      case 55:
        return "rain.jpg";
      case 61:
      case 63:
      case 65:
        return "rain.jpg";
      case 66:
      case 67:
        return "rain.jpg";
      case 71:
      case 73:
      case 75:
        return "snow.jpg";
      case 77:
        return "snow.jpg";
      case 80:
      case 81:
      case 82:
        return "rain.jpg";
      case 85:
      case 86:
        return "snow.jpg";
      case 95:
        return "thunderstorm.jpg";
      case 96:
      case 99:
        return "thunderstorm.jpg";
      default:
        return "Unknown";
    }
};
