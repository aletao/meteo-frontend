import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import "../node_modules/bootstrap/dist/js/bootstrap.bundle.js";
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import AuthContextProvider from './contexts/AuthContextProvider'
import HomePage from './pages/HomePage/HomePage'
import Navigation from './layouts/Navigation'
import RegisterPage from './pages/RegisterPage/RegisterPage.jsx';
import LoginPage from './pages/LoginPage/LoginPage.jsx';
import HistoryPage from './pages/HistoryPage/HistoryPage.jsx';

/* ALESSANDRO TAORMINA */
const router = createBrowserRouter([
  {
    element: (
      <AuthContextProvider>
        <Navigation />
      </AuthContextProvider>
    ),
    children: [
      {
        path: '/',
        element: <HomePage />
      },
      {
        path: '/register',
        element: <RegisterPage />
      },
      {
        path: '/login',
        element: <LoginPage />
      },
      {
        path: '/history',
        element: <HistoryPage />
      },
    ]
  }
])

ReactDOM.createRoot(document.getElementById('root')).render(
    <RouterProvider router={router}/>
)
