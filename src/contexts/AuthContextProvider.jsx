import { useState } from "react";
import { AuthContext } from "./AuthContext";

export default function AuthContextProvider({children}){

    const [user, setUser] = useState({})

    const [isAuth, setIsAuth]= useState(false)

    return(
        <>
            <AuthContext.Provider value={{user, setUser, isAuth, setIsAuth}}>
                {children}
            </AuthContext.Provider>
        </>
    )


}